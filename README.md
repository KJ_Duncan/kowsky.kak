I've gone and done a Kowsky! A backup file utility with [Kakoune](https://kakoune.org/) editor integration.  
  
```shell

#########
# Usage #
#########
"
kowsky cp source_file

          Copies a source file from the present working directory to kowsky's backup directory
          Hidden /.paths/ and /.files are ignored (no backup is created).

kowsky mv target_file

          Restores a corrupt or deleted file from the kowsky backup directory to the present working directory.
          The backup file remains intact. If target file was deleted from the working directory use:
             $ touch target_file
          to create an empty file which kowsky restores into.

kowsky [rm|--dry-run] [one|thirty|sixty|ninety|onetwenty]

          Removes the catalogue of backup files > days old.

kowsky rmdir

          Recursively remove backup files and kowsky created directories:
            /kak-back-kowsky/backups/*.*
"
######################
# Command Line Usage #
######################

# backup a file:
$ cd /into/the/directory/where/file/is/located
$ kowsky cp source_file

# restore a file:
$ cd /into/the/directory/where/file/was/deleted/or/corrupt
$ touch fileName.ext     # necessary if file was deleted
$ kowsky mv target_file  # backup file remains intact

# remove a catalogue of backup files > thirty days old:
$ kowsky rm thirty

# list files for removal > thirty days old:
$ kowsky --dry-run thirty

# remove all backup files and directory /kak-back-kowsky/*:
$ kowsky rmdir

##############################
# Kakoune Editor Integration #
##############################

# repositories kowsky.kakoune file
$ mv kowsky.kakoune $XDG_CONFIG_HOME/kak/autoload/kowsky.kak
# or
$ mv kowsky.kakoune $HOME/.config/kak/autoload/kowsky.kak

```


![](src/main/resources/png/sequence_diagram.png)


----


#### kowsky.kakoune


```shell

# does the heavy lifting
hook global BufWritePost .* %{
  nop %sh{ kowsky cp "${kak_hook_param}" }
}

# in editor convenience command on empty file
define-command kowsky-restore %{
  nop %sh{ kowsky mv "${kak_buffile}" }
} -docstring "restore current buffer file to its previously saved state"

# quick way to finish up at a foreign location
define-command kowsky-rmdir %{
  nop %sh{ kowsky rmdir }
} -docstring "recursively remove all /kak-back-kowsky/backups/*.*"

# remove > days old = one|thirty|sixty|ninety|onetwenty
hook global KakEnd .* %{
  nop %sh{ kowsky rm thirty }
}

# $KOWSKY_BACKUPS needs to be a /fully/qualified/directory/path not $HOME/path
define-command kowsky-24h -menu -params 1 -shell-script-candidates %{

  fd --show-errors --strip-cwd-prefix --exact-depth 1 --type file --changed-within 24h --base-directory $KOWSKY_BACKUPS | sed 's/%/\//g'

} %{ edit %arg{1} } -docstring "kowsky backup files modified between 0-24 hours"
# Reference
# AlMr 2021, mru-files.kak, gitlab, viewed 27 June 2021, <https://gitlab.com/kstr0k/mru-files.kak>
# sharkdp 2021, fd, github, viewed 27 June 2021 , <https://github.com/sharkdp/fd>
```


----


[![asciicast](https://asciinema.org/a/FoM3sABv4rc3KrsrSx9ubb3L0.svg)](https://asciinema.org/a/FoM3sABv4rc3KrsrSx9ubb3L0)  


----


#### References


-  Common Weakness Enumeration (CWE), 2020, _'CWE CATEGORY: File Handling Issues'_, Category ID: 1219, viewed 05 December 2020, https://cwe.mitre.org/data/definitions/1219.html
-  Kakoune 2020, _'5. Configuration & Autoloading'_, viewed 04 December 2020, https://github.com/mawww/kakoune#5-configuration--autoloading
-  Kakoune 2020, _'Debug Buffers'_, buffers.asciidoc, viewed 05 December 2020, https://github.com/mawww/kakoune/blob/master/doc/pages/buffers.asciidoc#debug-buffers
-  Oracle 2020, _'Default Policy Implementation and Policy File Syntax'_, Java Documentation, viewed 27 November 2020, https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
-  Oracle 2020, _'Files.isHidden'_, java.nio.file.Files, viewed 05 December 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/nio/file/Files.html
-  Oracle 2021, _'JEP 411: Deprecate the Security Manager for Removal'_, Java SE 17, viewed 18 September 2021, https://openjdk.java.net/jeps/411

 
---- 

 
Inspired by dmerejkowsky original post: [Create backup of each edited file](https://discuss.kakoune.com/t/create-backup-of-each-edited-file/1016), with source file [kak-back.rs](https://git.sr.ht/~dmerej/bwataout/tree/main/item/src/bin/kak-back.rs). Thanks Dmere.  
  
-  Merejkowsky, D 2020, _'skyspell'_, PyEnchant wrapper for Kakoune, viewed 14 July 2021, https://git.sr.ht/~dmerej/skyspell  
  
```text

@author Duncan, K & Merejkowsky, D 2020, KakBackKowsky.java & kak-back.rs, 
        🍵 418 - I'm a teapot, viewed 14 July 2021,
        https://bitbucket.org/KJ_Duncan/kowsky.kak/src/master/src/main/java/KakBackKowsky.java
        https://git.sr.ht/~dmerej/bwataout/tree/main/item/src/bin/kak-back.rs

@since Dizzee, R 2017, Wot U Gonna Do?, Raskit, viewed 07 June 2020,
       https://open.spotify.com/track/0XMS9iWCSKLunggRUsOd4C?si=7Uj93yQSS7q1O1dQWU3RxA
       https://youtu.be/8O0Spn2eaSk 

+----------------+----------------------+
|    Object      |       Type           |
+----------------+----------------------+
| kowsky         | executable           |
| jre-kowsky     | custom runtime image |
| kowsky.kak     | repository / module  |
| kowsky.kak.jar | archive for classes  |
| kowsky.kakoune | kakoune intergration |
+----------------+----------------------+

```


----


#### GraalVM native-image


```shell
$ mkdir -p build/classes build/libs 

$ $GRAALVM_HOME/bin/javac -d build/classes \
                          $(find src/main/java/ -name "*.java")

$ $GRAALVM_HOME/bin/jar --create \
                        --file build/libs/kowsky.kak.jar \
                        --main-class au.kjd.kowsky \
                        --module-version 0.3.1 \
                        -C build/classes/ .

$ $GRAALVM_HOME/bin/native-image --module-path build/libs/kowsky.kak.jar \
                                 -H:Module=kowsky.kak \
                                 -H:Name=kowsky \
                                 -H:+ReportExceptionStackTraces \
                                 --no-fallback

$ du -sh kowsky
> 12M    kowsky

$ chmod go-rwx kowsky
$ mv kowsky $HOME/local/bin

# Run the test suite
$ ./gradlew test --rerun-tasks
```


- Oracle 2022, _'Native Image'_, GraalVM, v22.2, viewed 28 August 2022, <https://www.graalvm.org/reference-manual/native-image/>


#### --module-path


@see package manager [_sdkman_](https://sdkman.io/) for installation instructions.  
  
```shell

$ mkdir -p build/classes build/libs 

$ javac -d build/classes \
        $(find src/main/java/ -name "*.java")

$ jar --create \
      --file build/libs/kowsky.kak.jar \
      --main-class au.kjd.kowsky \
      --module-version 0.2.0 \
      -C build/classes/ .

$ echo 'java --module-path /PATH/TO/KOWSKY/JAR --module kowsky.kak "$@"' > kowsky

$ chmod u-wx,go-rwx build/libs/kowsky.kak.jar && \
  chmod u+rx,u-w,go-rwx kowsky

$ mv build/libs/kowsky.kak.jar $HOME/YOUR/PATH/ && \ 
  mv kowsky $HOME/local/bin/
```
  
  
-  Deitel, P 2017, _'Understanding Java 9 Modules'_, What they are and how to use them, Java 9 Excerpt, Oracle, viewed 05 August 2020, https://www.oracle.com/corporate/features/understanding-java-9-modules.html  


#### jlink


```shell

$ jlink --output jrt-kowsky \
        --module-path $HOME/PATH/TO/JAR/kowsky:$JAVA_HOME/jmods \
        --add-modules kowsky.kak,java.base,java.logging \
        --compress=0 --no-man-pages

$ du -hs jrt-kowsky
>  51M   jrt-kowsky

# also need this to finish the job if using the below: ./gradlew jlink 
$ echo '$HOME/<PATH-TO>/jrt-kowsky/bin/java --module kowsky.kak "$@"' > kowsky

$ chmod -R go-rwx jrt-kowsky && \
  chmod u+rx,u-w,go-rwx kowsky

$ mv jrt-kowsky kowsky /somewhere/you/want/

```
  
  
-  Oracle 2020, _'The jlink Command'_, Java SE 15, viewed 20 November 2020, https://docs.oracle.com/en/java/javase/15/docs/specs/man/jlink.html  
-  OpenJDK 2017, _'JEP 220: Modular Run-Time Images'_, viewed 05 August 2020, https://openjdk.java.net/jeps/220  


#### gradle


Build with _'jlink - assemble and optimize a set of modules and their dependencies into a custom runtime image'_  
```shell

$ git clone https://bitbucket.org/KJ_Duncan/kowsky.kak.git
$ cd kowsky.kak

# bitbucket does not allow *.jar files in a git clone repo
$ gradle wrapper --gradle-version <VERSION> --distribution-type all
$ ./gradlew tasks --all
$ ./gradlew help --task jlink
# Other tasks
# -----------
$ ./gradlew jlink               # to build/jrt-kowsky-kak and build/libs/kowsky.kak.jar 
$ ./gradlew test --rerun-tasks  # to run test suite without the cache

# this is all you need: see the jlink 'echo' statement above to finish the job.
$ build/jrt-kowsky/bin/java --list-modules
> java.base@17
> java.logging@17
> kowsky.kak

# this jar is an optional extra.
$ ls build/libs/
> kowsky.kak.jar

$ jar --file build/libs/kowsky.kak.jar --describe-module
> kowsky.kak jar:file://$HOME/PATH/TO/kowsky.kak/build/libs/kowsky.kak.jar/!module-info.class
> requires java.base
> requires java.logging
> contains au.kjd
> main-class au.kjd.kowsky
```
  
-  Gradle 2020, _'The Gradle Wrapper'_, viewed 13 August 2020, https://docs.gradle.org/current/userguide/gradle_wrapper.html  
-  Ilya-g 2020, build.gradle.kts, kotlin-jlink-examples, gradle, viewed 01 March 2021, https://github.com/ilya-g/kotlin-jlink-examples/blob/master/gradle/app/build.gradle.kts
-  Sdkman 2020, _'Gradle'_, The Software Development Kit Manager, viewed 06 December 2020, https://sdkman.io/sdks#gradle  


#### jpackage


jpackage - tool for packaging self-contained Java applications.  
```shell
# to begin:
#   add jlink cli flags in build.gradle.kts
#   beneath "--output", outputDir, <-- remember to put the trailing ,
"--strip-debug",
"--no-header-files",
"--no-man-pages",
"--strip-native-commands"

# now run:
$ ./gradlew jlink && cd build/

# to build an appimage
$ jpackage --verbose \
           --type app-image \
           --name kowsky-app \
           --module kowsky.kak/au.kjd.kowsky \
           --runtime-image /YOUR/PATH/TO/kowsky.kak/build/jrt-kowsky \
           --java-options -XX:+DisableAttachMechanism \
           --java-options -XX:+UseSerialGC

$ du -sh kowsky-app
> 46M  kowsky-app

$ echo '/YOUR/PATH/TO/kowsky-app/bin/kowsky-app "$@"' > kowsky
$ chmod u+x,go-rwx kowsky && \
  mv kowsky $HOME/bin/local
```

- Oracle 2021, _'The jpackage Command'_, Java Development Kit Version 16 Tool Specifications, All Platforms, viewed 10 September 2021, https://docs.oracle.com/en/java/javase/16/docs/specs/man/jpackage.html


----


#### File Handling Issues


- CWE Category: File Handling Issues, https://cwe.mitre.org/data/definitions/1219.html
- CWE-23: Relative Path Traversal, https://cwe.mitre.org/data/definitions/23.html
- CWE-378: Creation of Temporary File With Insecure Permissions, https://cwe.mitre.org/data/definitions/378.html
- CWE-379: Creation of Temporary File in Directory with Insecure Permissions, https://cwe.mitre.org/data/definitions/379.html
- CWE-426: Untrusted Search Path, https://cwe.mitre.org/data/definitions/426.html
- CWE-427: Uncontrolled Search Path Element, https://cwe.mitre.org/data/definitions/427.html


----


#### Kakoune General Information


Work done? Have some fun. Share any improvements, ideas or thoughts with the community [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 230+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.

