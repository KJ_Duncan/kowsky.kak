# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.1 - 2022-09-18

- patch: shows parent directory [yyyy-mm-dd hh:mm:ss /dir/file1.ext] to stdout on kowsky rm and --dry-run

## 0.3.0 - 2021-09-18

- minor: JEP 411: Deprecate the Security Manager for Removal

## 0.2.2 - 2021-03-02

- patch: removed unrequired exports directive from module-info.java
- patch: gradle build script new task jlink builds a custom runtime image

## 0.2.1 - 2020-12-14

- patch: gradle build - the application plugin un*x/windows (for heuristic learning)


## 0.2.0 - 2020-12-12

- minor: kowsky new runAsync method runs all tasks on a worker thread (non-blocking kakoune integration)
- patch: gradle build script removed *.png file from resulting jar, no longer require the uberJar build
- patch: KakBackKowsky copyFile and moveFile renamed to hasCopied, hasMoved which both return boolean
- patch: kowsky new log methods for improved debugging, usage method improvements for console help
- patch: package-info.java file for gradle build requirement

## 0.1.2 - 2020-12-05

- patch: fixed kowsky rmdir call to run correct number of arguments

## 0.1.1 - 2020-12-04

- patch: Test suite expanded with two new file NoSecurityPermissionsTest and WithSecurityPermissionsTest
- patch: KakBackKowsky catches SecurityExceptions and reports details to user
- patch: KakBackKowsky refactor of Strings to static constants
- patch: Days minor refactor
- patch: kowsky new run method encapsulates decision tree logic

## 0.1.0 - 2020-11-27

- minor: KakBackKowsky.targetDir() checks caller has the correct path permissions else throws AccessControlException
- minor: KakBackKowsky checks a callers FilePermissions on read,write else throws AccessControlException
- patch: stdout file list is now sorted: kowsky.kak --dry-run
- patch: build.gradle.kts compiler Java SE 15
- patch: gradle-wrapper.properties version 6.7.1


