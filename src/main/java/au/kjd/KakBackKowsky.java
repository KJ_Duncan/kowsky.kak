package au.kjd;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.TERMINATE;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.isHidden;
import static java.nio.file.Files.setPosixFilePermissions;
import static java.nio.file.Files.walkFileTree;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.attribute.FileTime.from;
import static java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.nio.file.attribute.PosixFilePermissions.asFileAttribute;
import static java.time.Clock.offset;
import static java.time.Clock.systemDefaultZone;
import static java.time.Duration.ofDays;
import static java.time.Instant.now;
import static java.util.EnumSet.noneOf;
import static java.util.EnumSet.of;
import static java.util.logging.Logger.getLogger;
import static java.util.regex.Pattern.compile;

final class KakBackKowsky {

  private static final Logger LOGGER  = getLogger(KakBackKowsky.class.getSimpleName());

  private static final String TZ      = "[TZ]";
  private static final String SPACE   = " ";
  private static final String FORMAT  = "%1$s%2$s%n";
  private static final String PERCENT = "%";

  private static final String USER_HOME   = "user.home";
  private static final String BACKUP_PATH = ".local/share/kak-back-kowsky/backups/";

  private static final Pattern SLASH = compile("/");

  private static final CopyOption[] OPTIONS = { REPLACE_EXISTING };

  private static final Set<PosixFilePermission> USER_RW  = of(OWNER_READ, OWNER_WRITE);
  private static final Set<PosixFilePermission> USER_RWX = of(OWNER_READ, OWNER_WRITE, OWNER_EXECUTE);

  /* runs a create directory check /kak-back-kowsky/backups */
  private KakBackKowsky() { mkdir(); }

  // ------------------- Package Level Access ------------------- //

  /* package-private static method instantiates the private class */
  static KakBackKowsky init() { return new KakBackKowsky(); }

  /* saves current buffer file via kakoune's write on save hook to kak-back-kowsky/backups/* */
  boolean hasCopied(final String from) { return cp(Path.of(from)); }

  /* restores the current buffer file with its namesake's previously saved state in kak-back-kowsky/backups/* */
  boolean hasMoved(final String to) { return mv(Path.of(to)); }

  /* walks kak-back-kowsky/backups/*.* and returns a list of removed files based on number of days from last modified time */
  List<String> removeBackups(final Days days) { return rm(days); }

  /* walks kak-back-kowsky/backups/*.* and returns a list of files marked for removal based on number of days from last modified time */
  List<String> removeDryRun(final Days days) { return doDryRun(days); }

  /* leaves no trace: /kak-back-kowsky/backups/*.* */
  boolean removeKowskyDir() { return emptyDir(); }

  @Override protected Object clone() throws CloneNotSupportedException { throw new CloneNotSupportedException(); }

  // ------------------- Encapsulation of Logic ------------------- //

  /* helper function returns backup path else throws IOException */
  private Path targetDir() { return get(getProperty(USER_HOME), BACKUP_PATH); }

  /* if directory exists continue else create */
  private void mkdir() {

    try { createDirectories(targetDir(), posix()); }
    catch (IOException ie) { LOGGER.severe(format("Unable to create backup directory: %s", ie.getMessage())); }
  }

  /* sets file permissions rw-------, directory permissions rwx------  */
  private FileAttribute<Set<PosixFilePermission>> posix() { return asFileAttribute(KakBackKowsky.USER_RWX); }

  /* replace file separator '/' to '%' */
  private Path resolveSourceFile(final Path source) throws IOException {

    return Optional.of(source.toRealPath(NOFOLLOW_LINKS))
                   .map(input   -> SLASH.matcher(input.toString()))
                   .map(matcher -> matcher.replaceAll(PERCENT))
                   .map(str     -> targetDir().resolve(str)).orElseThrow();
  }

  /* copy a to b, if a is not on hidden path or file */
  private boolean cp(final Path a) {

    boolean copied = false;

    try { if (!isHidden(a)) {
              final Path b = resolveSourceFile(a);
              copy(a, b, OPTIONS);
              setPosixFilePermissions(b, USER_RW);
              copied = true; } }

    catch (IOException ie) { LOGGER.severe(format("Unable to copy: %s %s", a.getFileName(), ie.getMessage())); }

    return copied;
  }

  /* copy b to a, where b = f(a); backup file remains intact */
  private boolean mv(final Path a) {

    boolean moved = false;

    try { copy(resolveSourceFile(a), a, OPTIONS);
          setPosixFilePermissions(a, USER_RW);
          moved = true; }

    catch (IOException ie) { LOGGER.severe(format("Unable to move: %s %s", a.getFileName(), ie.getMessage())); }

    return moved;
  }

  /* if days ago (Long) > (Long) last modified time then delete else continue
     Long.valueOf(x).compareTo(Long.valueOf(y))
     if x == y then 0, if x < y then -1, if x > y then 1
   */
  private List<String> rm(final Days days) {

    final List<String> files = new ArrayList<String>(350);

    try { walkFileTree(targetDir(), noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {

            @Override public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {

              // if number of days ago is greater than the last modified time then 1 else -1
              // 0 < 1
              if (0 < numberOfDaysAgo(days).compareTo(attrs.lastModifiedTime())) {

                  files.add(prettyPrint(parseTime(attrs.lastModifiedTime().toString()),
                                        parsePath(file.getFileName().toString())));
                  delete(file); }

              return CONTINUE; }}); }

    catch (IOException ie) { LOGGER.warning(format("Unable to remove backup file: %s", ie.getMessage())); }

    files.sort(Comparator.naturalOrder());

    return files;
  }

  /* no deletion occurs on walk /backups/- */
  private List<String> doDryRun(final Days days) {

    final List<String> files = new ArrayList<String>(350);

    try { walkFileTree(targetDir(), noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {

            @Override public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {

              if (0 < numberOfDaysAgo(days).compareTo(attrs.lastModifiedTime())) {

                  files.add(prettyPrint(parseTime(attrs.lastModifiedTime().toString()),
                                        parsePath(file.getFileName().toString()))); }

              return CONTINUE; }}); }

    catch (IOException ie) { LOGGER.warning(format("Unable to lookup backup file: %s", ie.getMessage())); }

    files.sort(Comparator.naturalOrder());

    return files;
  }

  /* remove all files in /backups/*.* then remove /backups directory and its parent /kak-back-kowsky */
  private boolean emptyDir() {

    boolean ok = false;

    try { walkFileTree(targetDir(), noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {

            @Override public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
              delete(file);
              return CONTINUE;
            }

            @Override public FileVisitResult postVisitDirectory(final Path dir, final IOException ioe) throws IOException {
              Path parent = dir.getParent();
              delete(dir);
              delete(parent);
              return TERMINATE;
            }
          }); ok = true; }

    catch (IOException ie) { LOGGER.severe(format("Unable to remove directory kak-back-kowsky: %s", ie.getMessage())); }

    return ok;
  }

  /* NOTE: negation -(days)  */
  private FileTime numberOfDaysAgo(final Days days) {

    return from(now(offset(systemDefaultZone(),
                    ofDays(-days.getDays()))));
  }

  /* yyyy-mm-ddThh:mm:ssZ -> yyyy-mm-dd hh:mm:ss */
  private String parseTime(final String str) {

    return Optional.of(compile(TZ))
                   .map(p -> p.matcher(str))
                   .map(m -> m.replaceAll(SPACE)).get();
  }

  /* %Path%to%dir%file.ext -> %dir%file.ext -> /dir/file.ext */
  private String parsePath(final String str) { return str.substring(str.lastIndexOf('%', str.lastIndexOf('%')-1)).replace('%', '/'); }

  /*
     [yyyy-mm-dd hh:mm:ss /dir/file1.ext
      yyyy-mm-dd hh:mm:ss /dir/file2.ext
      yyyy-mm-dd hh:mm:ss /dir/file3.ext]
   */
  private String prettyPrint(final String time, final String path) { return format(FORMAT, time, path); }

}
