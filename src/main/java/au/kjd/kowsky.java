package au.kjd;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;
import java.util.logging.Logger;

import static au.kjd.KakBackKowsky.init;
import static java.lang.Boolean.TRUE;
import static java.lang.Integer.MIN_VALUE;
import static java.lang.Runtime.getRuntime;
import static java.lang.String.format;
import static java.lang.System.err;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.logging.Logger.getLogger;

/**
 * I've gone and done a Kowsky!
 *
 *  backup=cp, restore=mv, clean=rm,  clean-all=rmdir, --dry-run
 *
 * @author Duncan, K & Merejkowsky, D 2020, KakBackKowsky.java & kak-back.rs,
 *         🍵 418 - I'm a teapot, viewed 03 June 2020,
 *         https://bitbucket.org/KJ_Duncan/kowsky.kak/src/master/src/main/java/KakBackKowsky.java
 *         https://github.com/dmerejkowsky/dotfiles/blob/master/dm-tools/src/bin/kak-back.rs
 *
 * @since Dizzee, R 2017, Wot U Gonna Do?, Raskit, viewed 07 June 2020,
 *        https://open.spotify.com/track/0XMS9iWCSKLunggRUsOd4C?si=7Uj93yQSS7q1O1dQWU3RxA
 *        https://youtu.be/8O0Spn2eaSk
 */
final class kowsky {

  private static final Logger LOGGER = getLogger(kowsky.class.getSimpleName());

  private static final long TIMEOUT = 10L;

  private static final String CP      = "cp";
  private static final String MV      = "mv";
  private static final String RM      = "rm";
  private static final String RMDIR   = "rmdir";
  private static final String DRY_RUN = "--dry-run";

  private static final String THREAD_EX = "kowsky thread error";

  /* constructor */
  private kowsky() { }

  @Override protected Object clone() throws CloneNotSupportedException { throw new CloneNotSupportedException(); }

  /* backup catalogue days */
  private static Days olderThan(final String days) {

    try { return Days.valueOf(days.toUpperCase()); }

    catch (IllegalArgumentException|
           NullPointerException ine) { return Days.ONETWENTY; }
  }

  /* future task runs on worker thread */
  private static BiFunction<String, String, Future<Boolean>> runAsync() {

    return (option, argument) -> Executors.newSingleThreadExecutor()
                                          .submit(() -> run(option, argument));
  }

  /* decision tree based on user option and argument
   * Boolean return type is a dummy value for runAsync
   */
  private static Boolean run(final String option, final String argument) {

    switch (option) {
      case CP      -> init().hasCopied(argument);
      case MV      -> log(init().hasMoved(argument));
      case RM      -> log(init().removeBackups(olderThan(argument)));
      case RMDIR   -> log(init().removeKowskyDir());
      case DRY_RUN -> log(init().removeDryRun(olderThan(argument)));
      default      -> usage(); }

    return TRUE;
  }

  /* wrapper for console output */
  private static void log(final Object any) { err.println(any); err.flush(); }

  /* wrap: log(init().hasCopied(argument)) to see output in kakoune debug buffer */
  private static void log(final boolean any) { LOGGER.info(format("kowsky=%s", any)); }

  /**
   * Command line usage.
   *
   * @author Oracle 2020, Default Policy Implementation and Policy File Syntax,
   *         Java Documentation, viewed 27 November 2020,
   *         https://docs.oracle.com/en/java/javase/15/security/permissions-jdk1.html
   *
   * @author The Open Group 2018, 'Utility Conventions',
   *         12.1 Utility Argument Syntax, The Open Group Base Specifications Issue 7,
   *         2018 edition IEEE Std 1003.1-2017 (Revision of IEEE Std 1003.1-2008),
   *         viewed 26 Feb 2020, https://pubs.opengroup.org/onlinepubs/9699919799/
   */
  private static void usage() {
    err.println("""
      usage: kowsky cp source_file
                       Copies a source file from the present working directory to kowsky's backup directory
                       Hidden /.paths/ and /.files are ignored (no backup is created).

             kowsky mv target_file
                       Restores a corrupt or deleted file from the kowsky backup directory to the present working directory.
                       The backup file remains intact. If target file was deleted from the working directory use:
                          $ touch target_file
                       to create an empty file which kowsky restores into.

             kowsky [rm|--dry-run] [one|thirty|sixty|ninety|onetwenty]
                       Removes the catalogue of backup files > days old.

             kowsky rmdir
                       Recursively remove backup files and kowsky created directories:
                         /kak-back-kowsky/backups/*.*
    """);
    err.flush();
  }

  /**
   * Prefix commands with `kowsky`
   *   cp file.ext
   *   mv file.ext
   *   rm one|thirty|sixty|ninety|onetwenty
   *   --dry-run one|thirty|sixty|ninety|onetwenty
   *
   * Recursively removes all files and the directory structure: /kak-back-kowsky/backups/*.*
   *   rmdir
   *
   * @param args cp, mv, rm, rmdir, --dry-run
   */
  public static void main(final String[] args) {

    if (MIN_VALUE == args.length + 1 ||
        0 == args.length) { usage(); getRuntime().exit(-1); }

    try { final String[] clone = args.clone();

          runAsync().apply(clone[0], (1 == clone.length) ? "" : clone[1])
                    .get(TIMEOUT, SECONDS); }

    catch (CancellationException|
           ExecutionException|
           InterruptedException|
           TimeoutException ceit) { LOGGER.severe(format("%s: %s",
                                                         THREAD_EX,
                                                         ceit.getMessage())); }

    finally { getRuntime().exit(0); }
  }
}
