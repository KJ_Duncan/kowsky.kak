package au.kjd;

/* type safe enumeration for the number of days > last modified file time */
enum Days {
  ONE(1L),
  THIRTY(30L),
  SIXTY(60L),
  NINETY(90L),
  ONETWENTY(120L);

  Days(final long days) { this.days = days; }

  private final long days;
  long getDays() { return days; }
}
