package au.kjd;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import static java.nio.file.attribute.FileTime.from;
import static java.time.Clock.offset;
import static java.time.Clock.systemDefaultZone;
import static java.time.Duration.ofDays;
import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KakBackKowskyTest {

  @TempDir
  static Path sharedTempDir;

  @Test
  @DisplayName("Replace source path '/' with target path '%' preserving path length")
  void resolveSourceFile() throws IOException {
    Files.createTempFile(sharedTempDir, "test", "txt");
    Path file = sharedTempDir.resolve("test.txt");

    Pattern slash = Pattern.compile("/");
    Matcher matcher = slash.matcher(file.toString());
    String str = matcher.replaceAll("%");

    assertEquals(file.toString().length(), str.length());
    assertThat(str).doesNotContain("/").contains("%");
  }

  @Test
  @DisplayName("Walk the temp directory and collect a file list with size >= 3")
  void listRemovedFiles() throws IOException {
    Files.createTempFile(sharedTempDir, "one", "txt");
    Files.createTempFile(sharedTempDir, "two", "txt");
    Files.createTempFile(sharedTempDir, "three", "txt");

    List<String> files = new ArrayList<String>(5);

    /* if days ago (Long) < (Long) last modified time then delete else continue
       Long.valueOf(x).compareTo(Long.valueOf(y))
       if x == y then 0, if x < y then -1, if x > y then 1 */
    Files.walkFileTree(sharedTempDir, EnumSet.noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        // is 30 days ago > the last modified time then 1
        // is 30 days ago < the last modified time then -1
        if (0 > from(now(offset(systemDefaultZone(),
          ofDays(-30L)))).compareTo(attrs.lastModifiedTime())) {
          files.add(file.getFileName().toString());
        }
        return FileVisitResult.CONTINUE;
      }
    });
    assertThat(files).hasSizeGreaterThanOrEqualTo(3);
  }

//  https://github.com/junit-team/junit5/blob/r5.6.2/junit-jupiter-engine/src/main/java/org/junit/jupiter/engine/extension/TempDirectory.java
//  @EnabledOnOs(MAC)
//  @DisplayName("Walk the temp directory and return its parent 'T' for OSX. Test ignored on LINUX as temp dir location unknown")
//  assertThat(list.get(1)).contains("T");

  @Test
  @DisplayName("Walk the temp directory and return its parent")
  void rmdir() throws IOException {

    List<String> list = new ArrayList<String>(5);
    Pattern pattern = Pattern.compile("/");

    Files.walkFileTree(sharedTempDir, EnumSet.noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
        // realpath /the/whole/stem/and/leaf
        list.add(dir.getParent().toString());
        // parent directory name leaf
        list.add(dir.getParent().getFileName().toString());
        return FileVisitResult.TERMINATE;
      }
    });
    assertThat(list).hasSize(2);
    assertThat(list.get(0)).containsPattern(pattern);
    assertThat(list.get(1)).isNotEmpty();
  }

  @Test
  @DisplayName("Copy source to target charset utf-8 contents are equivalent")
  void cp() throws IOException {
    Files.createTempFile(sharedTempDir, "source", "txt");
    Path a = sharedTempDir.resolve("source.txt");

    Pattern slash = Pattern.compile("/");
    Matcher matcher = slash.matcher(a.toString());
    String str = matcher.replaceAll("%");

    Path b = sharedTempDir.resolve(str);

    Charset charset = Charset.forName("UTF-8");
    String msg = "Some utf 8 text";

    // a try-with-resources statement (auto-closable)
    try (BufferedWriter writer = Files.newBufferedWriter(a, charset)) {
      writer.write(msg, 0, msg.length());
    } catch (IOException ioe) { System.err.format("IOException: %s%n", ioe); }

    Files.copy(a, b, StandardCopyOption.REPLACE_EXISTING);

    assertThat(a).hasSameTextualContentAs(b, charset);
  }

  @Test
  @DisplayName("Target file permissions are user only read write rw-------")
  void mv() throws IOException {
    Files.createTempFile(sharedTempDir, "target", "txt");
    Path b = sharedTempDir.resolve("target.txt");

    Charset charset = Charset.forName("UTF-8");
    String msg = "Some utf 8 text";

    // a try-with-resources statement (auto-closable)
    try (BufferedWriter writer = Files.newBufferedWriter(b, charset)) {
      writer.write(msg, 0, msg.length());
    } catch (IOException ioe) { System.err.format("IOException: %s%n", ioe); }

    Set<PosixFilePermission> user_rw = EnumSet.of(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE);
    Files.setPosixFilePermissions(b, user_rw);

    assertThat(b).isRegularFile().usingCharset(charset).isReadable().isWritable();
    assertThat(user_rw).isEqualTo(Files.getPosixFilePermissions(b));
  }

  @Test
  @DisplayName("Parse last modified time and file path removing 'T' 'Z' '%'")
  void parseAndPrettyPrint() throws IOException {
    Files.createTempFile(sharedTempDir, "seven", ".txt");
    Files.createTempFile(sharedTempDir, "eight", ".txt");
    Files.createTempFile(sharedTempDir, "nine", ".txt");

    List<String> files = new ArrayList<String>(5);

    Pattern slash = Pattern.compile("/");
    Pattern tz = Pattern.compile("[TZ]");

    /* if days ago (Long) < (Long) last modified time then delete else continue
       Long.valueOf(x).compareTo(Long.valueOf(y))
       if x == y then 0, if x < y then -1, if x > y then 1 */
    Files.walkFileTree(sharedTempDir, EnumSet.noneOf(FileVisitOption.class), 1, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
        // is 30 days ago > the last modified time then 1
        // is 30 days ago < the last modified time then -1
        if (0 > from(now(offset(systemDefaultZone(),
          ofDays(-30L)))).compareTo(attrs.lastModifiedTime())) {

          String time = attrs.lastModifiedTime().toString();
          Matcher matches = tz.matcher(time);
          String dt = matches.replaceAll(" ");

          String path = file.toString();
          Matcher matcher = slash.matcher(path);
          String str = matcher.replaceAll("%");
          String name = str.substring(str.lastIndexOf('%', str.lastIndexOf('%')-1)).replace('%', '/');
          // String name = str.substring(str.lastIndexOf('%')+1);
          files.add(String.format("%1$s%2$s%n", dt, name));
        }
        return FileVisitResult.CONTINUE;
      }
    });

    /*
    for (String file : files) { System.out.format("%s", file); }
    System.out.flush();
    */

    assertThat(files)
      .doesNotContainNull()
      .doesNotContain("%", "T", "Z")
      .doesNotContainSequence("%", "T", "Z")
      .doesNotContainSubsequence("%", "T", "Z")
      .hasSizeGreaterThanOrEqualTo(3);
  }
}
