package au.kjd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DaysTest {

  @Test
  void days_not_null() {
    assertNotNull(Days.values());
  }

  @Test
  void Days_getDays_returns_correct_long_number() {
    assertEquals(1L, Days.ONE.getDays());
    assertEquals(30L, Days.THIRTY.getDays());
    assertEquals(60L, Days.SIXTY.getDays());
    assertEquals(90L, Days.NINETY.getDays());
    assertEquals(120L, Days.ONETWENTY.getDays());
  }

  @Test
  void Days_values_is_length_5() {
    assertEquals(5, Days.values().length);
  }

  @Test
  void Days_valueOf_is_ONE_THIRTY_SIXTY_NINETY_ONETWENTY() {
    assertEquals(Days.ONE, Days.valueOf("one".toUpperCase()));
    assertEquals(Days.THIRTY, Days.valueOf("thirty".toUpperCase()));
    assertEquals(Days.SIXTY, Days.valueOf("sixty".toUpperCase()));
    assertEquals(Days.NINETY, Days.valueOf("ninety".toUpperCase()));
    assertEquals(Days.ONETWENTY, Days.valueOf("onetwenty".toUpperCase()));
  }
}
