package au.kjd;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiFunction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class KowskyTest {

  @Test
  @DisplayName("runAsync should throw TimeoutException after 10 milli-seconds")
  final void runAsyncTest() throws Exception {
    /*
       CancellationException
       ExecutionException
       InterruptedException
       TimeoutException
     */
    final String OPTION = "cp";
    final String ARGUMENT = "test.txt";
    final long TIMEOUT_MILLISECONDS = 10L;

    final BiFunction<String, String, Boolean> run = (option, argument) -> Boolean.TRUE;

    final BiFunction<String, String, Boolean> runWithTimeout = (option, argument) -> {
      try { Thread.sleep(20L); return Boolean.TRUE; } catch (InterruptedException e) { }
      return Boolean.FALSE;
    };

    final BiFunction<String, String, Future<Boolean>> runAsync =
      (option, argument) -> Executors.newSingleThreadExecutor().submit(() -> run.apply(option, argument));

    final BiFunction<String, String, Future<Boolean>> runAsyncWithTimeout =
      (option, argument) -> Executors.newSingleThreadExecutor().submit(() -> runWithTimeout.apply(option, argument));

    final BiFunction<String, String, Future<Boolean>> runAsyncWithThreadSleepReturnTrue =
      (option, argument) -> Executors.newSingleThreadExecutor().submit(() -> runWithTimeout.apply(option, argument));

    assertDoesNotThrow(
      () -> runAsync.apply(OPTION, ARGUMENT).get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS));

    assertThrows(TimeoutException.class,
      () -> runAsyncWithTimeout.apply(OPTION, ARGUMENT).get(TIMEOUT_MILLISECONDS, TimeUnit.MILLISECONDS));

    assertEquals(Boolean.TRUE, runAsync.apply(OPTION, ARGUMENT).get());
    assertEquals(Boolean.TRUE, runAsyncWithThreadSleepReturnTrue.apply(OPTION, ARGUMENT).get());
  }
}
