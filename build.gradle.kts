plugins { `java-library` }

group = "au.kjd.kowsky.kak"
version = "0.3.1"

val moduleName by extra("kowsky.kak")
val javaHome = System.getProperty("java.home")

repositories { mavenCentral() }

dependencies {
  testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")
  testImplementation("org.assertj:assertj-core:3.23.1")
}

tasks {
  /* https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_modular
   * Gradle - Building Modules for the Java Module System
   *        - Java Module System support is an incubating feature */
  compileJava {
    options.compilerArgs = listOf("--release", "18")
    options.javaModuleMainClass.set("au.kjd.kowsky")
    java.modularity.inferModulePath.set(true)
  }

  test {
    useJUnitPlatform()
    jvmArgs = listOf("-enableassertions", "--enable-preview")
    testLogging.showStandardStreams = true
    testLogging {
      events(
        "passed",
        "failed",
        "skipped"
      )
    }
  }

  jar {
    enabled = true

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".png") ||
      details.file.name.endsWith(".puml")
    }

    /* need to rerun:
     *  $ jar --update --file build/libs/kowsky.kak.jar --main-class au.kjd.kowsky
     */
    manifest {
      attributes(
        "Manifest-Version" to "2.2",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.kowsky",
        "Specification-Title" to "Kowsky a backup file utility",
        "Specification-Version" to project.version
      )
    }
  }

  /* ilya-g 2020, build.gradle.kts, kotlin-jlink-examples, gradle, viewed 01 March 2021,
   *              https://github.com/ilya-g/kotlin-jlink-examples/blob/master/gradle/app/build.gradle.kts
   */
  register<Exec>("jlink") {
    description = "Build kowsky.kak module jar with an optimised custom runtime image"
    val outputDir by extra("$buildDir/jrt-kowsky")
    inputs.files(configurations.runtimeClasspath)
    inputs.files(jar)
    outputs.dir(outputDir)
    doFirst {
      val modulePath = files(jar) + configurations.runtimeClasspath.get()
      logger.lifecycle(modulePath.joinToString("\n", "jlink module path:\n"))
      logger.lifecycle("\njlink custom runtime:\n ${outputDir}\n")
      delete(outputDir)
      commandLine("$javaHome/bin/jlink",
                  "--module-path",
                  listOf("$javaHome/jmods/", modulePath.asPath).joinToString(File.pathSeparator),
                  "--add-modules", moduleName,
                  "--output", outputDir
      )
    }
  }
}
